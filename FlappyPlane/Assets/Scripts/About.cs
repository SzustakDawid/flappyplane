﻿using UnityEngine;
using System.Collections;

public class About : MonoBehaviour {
	
	public float guiPlacementY1;
	public float guiPlacementX1;
	public Texture backgroundTexture; 

	public float textY1;
	public float textX1;
	
	void OnGUI()
	{		
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height),backgroundTexture);

		GUIStyle customButton = new GUIStyle("button");
		customButton.fontSize = (int)(Screen.height * 0.05f);
		
		var gs = new GUIStyle ();
		gs.fontSize = (int)(Screen.width * .05f);
		gs.fontStyle = FontStyle.Bold;
		gs.normal.textColor = Color.black;
	
		GUI.Label (new Rect (Screen.width * textX1, Screen.height * 0.10f, Screen.width * .5f, Screen.height * .1f), "Gra stworzona przy pomocy silnika", gs);
		GUI.Label (new Rect (Screen.width * textX1 + 20, Screen.height * 0.20f, Screen.width * .5f, Screen.height * .1f), "Unity v 5.0.1f1 Personal", gs);

		GUI.Label (new Rect (Screen.width * textX1, Screen.height * 0.40f, Screen.width * .3f, Screen.height * .1f), "Autor gry", gs);
		GUI.Label (new Rect (Screen.width * textX1 + 20, Screen.height *  0.5f, Screen.width * .5f, Screen.height * .1f), "Dawid Szustak", gs);

		if (GUI.Button (new Rect (Screen.width * guiPlacementX1, Screen.height * guiPlacementY1, Screen.width * .5f, Screen.height * .1f), "Powrót", customButton))
		{
			Application.LoadLevel("MainMenu");
		}		
	}

	void Update()
	{
		if (Input.GetKeyUp (KeyCode.Escape)) {
			Application.LoadLevel("MainMenu");
		}
	}
}
