﻿using Assets.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameOver : MonoBehaviour
{

    public float guiPlacementY1;
    public float guiPlacementX1;
    public Texture backgroundTexture;

    public float textY1;
    public float textX1;

    public float textY2;
    public float textX2;

    public float guiPlacementY2;

    void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backgroundTexture);

        GUIStyle customButton = new GUIStyle("button");
        customButton.fontSize = (int)(Screen.height * 0.05f);

        var gs = new GUIStyle();
        gs.fontSize = (int)(Screen.width * 0.09f);
        gs.fontStyle = FontStyle.Bold;
        gs.normal.textColor = Color.black;

        GUI.Label(new Rect(Screen.width * textX1, Screen.height * textY1, Screen.width * .5f, Screen.height * .1f), "Game Over", gs);

        GUI.Label(new Rect(Screen.width * textX2, Screen.height * textY2, Screen.width * .5f, Screen.height * .1f), "Score: " + HighScoreHelper.ActualScore, gs);

        if (GUI.Button(new Rect(Screen.width * guiPlacementX1, Screen.height * guiPlacementY1, Screen.width * .5f, Screen.height * .1f), "Jeszcze raz", customButton))
        {
            SaveScore();
            Application.LoadLevel("Play");
        }

        if (GUI.Button(new Rect(Screen.width * guiPlacementX1, Screen.height * guiPlacementY2, Screen.width * .5f, Screen.height * .1f), "Menu", customButton))
        {
            SaveScore();
            Application.LoadLevel("MainMenu");
        }
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            SaveScore();
            Application.LoadLevel("MainMenu");
        }
    }

    private void SaveScore()
    {
        var highScore = PlayerPrefs.GetString("HighScore");
        if (string.IsNullOrEmpty(highScore))
        {
            Debug.Log("Nie istnieje lista HighScore");
            var hs = new List<Score>();
            hs.Add(new Score()
                {
                    Time = DateTime.Now,
                    User = PlayerPrefs.GetString("UserName"),
                    UserScore = HighScoreHelper.ActualScore
                });
            string serialized = HighScoreHelper.SerializeObject<List<Score>>(hs);
            PlayerPrefs.SetString("HighScore", serialized);
        }
        else
        {
            Debug.Log("Istnieje lista HighScore");
            List<Score> hs = (List<Score>)HighScoreHelper.DeserializeObject<List<Score>>(highScore);
            Debug.Log(hs.Count);
            hs.Add(new Score()
                {
                    Time = DateTime.Now,
                    User = PlayerPrefs.GetString("UserName"),
                    UserScore = HighScoreHelper.ActualScore
                });
            hs = hs.OrderByDescending(x => x.UserScore).ToList();
            if (hs.Count > 5)
            {
                hs = hs.Take(5).ToList();
            }
            var serialized = HighScoreHelper.SerializeObject(hs);
            PlayerPrefs.SetString("HighScore", serialized);
        }
        PlayerPrefs.Save();
    }
}