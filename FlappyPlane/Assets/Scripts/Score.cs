﻿using System;

public class Score
{
    public int UserScore { get; set; }
    public string User { get; set; }
    public DateTime Time { get; set; }
}
