﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class CreateUser : MonoBehaviour
    {

        public float guiPlacementY1;
        public float guiPlacementX1;
        public Texture backgroundTexture;
        public UnityEngine.UI.InputField inputField;
        private string _userName;

        void OnGUI()
        {
            GUIStyle customButton = new GUIStyle("button");
            customButton.fontSize = (int)(Screen.height * 0.05f);

            var gs = new GUIStyle();
            gs.fontSize = (int)(Screen.width * .05f);
            gs.fontStyle = FontStyle.Bold;
            gs.normal.textColor = Color.black;

            var txtObj = GameObject.Find("/Canvas/InputField/Text").GetComponent<Text>();

            inputField.ActivateInputField();
            inputField.useGUILayout = true;

            if (GUI.Button(new Rect(Screen.width * guiPlacementX1, Screen.height * guiPlacementY1, Screen.width * .5f, Screen.height * .1f), "Zapisz", customButton))
            {
                _userName = txtObj.text;
                if (!string.IsNullOrEmpty(_userName))
                {
                    PlayerPrefs.SetString("UserName", _userName);
                    Application.LoadLevel("MainMenu");
                }
            }
        }
    }
}