﻿using Assets.Scripts;
using System.Collections.Generic;
using UnityEngine;

public class HighScores : MonoBehaviour
{
    public float guiPlacementY1;
    public float guiPlacementX1;
    public Texture backgroundTexture;

    public float textY1;
    public float textX1;

    public float scoreX1;
    public float scoreY1;

    void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backgroundTexture);

        GUIStyle customButton = new GUIStyle("button");
        customButton.fontSize = (int)(Screen.height * 0.05f);

        var gs = new GUIStyle();
        gs.fontSize = (int)(Screen.width * .05f);
        gs.fontStyle = FontStyle.Bold;
        gs.normal.textColor = Color.black;

        string hs = PlayerPrefs.GetString("HighScore");
        List<Score> listOfScore = new List<Score>();
        if (!string.IsNullOrEmpty(hs))
        {
            listOfScore = (List<Score>)HighScoreHelper.DeserializeObject<List<Score>>(hs);
        }

        if (listOfScore.Count == 0)
        {
            GUI.Label(new Rect(Screen.width * textX1, Screen.height * textY1, Screen.width * .5f, Screen.height * .1f), "Brak rekordów", gs);
        }
        else
        {
            for (int i = 0; i < listOfScore.Count; i++)
            {
                var score = listOfScore[i];
                double x = 0.03;
                double y = 0.1;
                if (i > 0)
                {
                    y = y * (i + 1);
                }

                if (i == 0)
                {
                    gs = new GUIStyle();
                    gs.fontSize = (int)(Screen.width * .05f);
                    gs.fontStyle = FontStyle.Bold;
                    gs.normal.textColor = new Color32(218, 165, 32, 255);
                }
                else if (i == 1)
                {
                    gs = new GUIStyle();
                    gs.fontSize = (int)(Screen.width * .05f);
                    gs.fontStyle = FontStyle.Bold;
                    gs.normal.textColor = Color.grey;
                }
                else if (i == 2)
                {
                    gs = new GUIStyle();
                    gs.fontSize = (int)(Screen.width * .05f);
                    gs.fontStyle = FontStyle.Bold;
                    gs.normal.textColor = new Color32(91, 57, 30, 255);
                }
                else
                {
                    gs = new GUIStyle();
                    gs.fontSize = (int)(Screen.width * .05f);
                    gs.fontStyle = FontStyle.Bold;
                    gs.normal.textColor = Color.black;
                }

                GUI.Label(new Rect(Screen.width * (float)x, Screen.height * (float)y, Screen.width * .5f, Screen.height * .1f), string.Format("{0,-4} {1,-9} Pkt: {2,-5} {3}", i + 1, score.User, score.UserScore, score.Time.ToString("HH:mm dd-MM-yyyy")), gs);
            }
        }

        if (GUI.Button(new Rect(Screen.width * guiPlacementX1, Screen.height * guiPlacementY1, Screen.width * .5f, Screen.height * .1f), "Powrót", customButton))
        {
            Application.LoadLevel("MainMenu");
        }
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Application.LoadLevel("MainMenu");
        }
    }
}
