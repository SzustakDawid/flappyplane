﻿using Assets.Scripts;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Vector2 jumpForce = new Vector2(0, 300);

    void Update()
    {

        if (Input.GetKeyUp(KeyCode.Space) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            GetComponent<Rigidbody2D>().AddForce(jumpForce);
        }

        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Application.LoadLevel("MainMenu");
        }

        Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        if (screenPosition.y > Screen.height || screenPosition.y < 0)
        {
            Die();
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        Die();
    }

    void Die()
    {
        HighScoreHelper.ActualScore = Generate.Score;
        Application.LoadLevel("GameOver");
    }
}
