﻿using Assets.Scripts;
using System.Collections.Generic;
using UnityEngine;

public class Generate : MonoBehaviour
{
    public static int Score { get; set; }
    public List<Score> listOfScores { get; set; }

    public GameObject rocks;
    int score = 0;

    void Start()
    {
        InvokeRepeating("CreateObstacle", 1f, 1.5f);
    }

    void OnGUI()
    {
        if (listOfScores == null)
        {
            string serialized = PlayerPrefs.GetString("HighScore");
            if (string.IsNullOrEmpty(serialized))
            {
                listOfScores = new List<Score>();
            }
            else
            {
                listOfScores = (List<Score>)HighScoreHelper.DeserializeObject<List<Score>>(serialized);
            }
        }

        GUIStyle customButton = new GUIStyle("button");
        customButton.fontSize = (int)(Screen.height * 0.05f);


        if (listOfScores.Count > 2)
        {
            if (listOfScores[0].UserScore < score)
                GUI.color = new Color32(218, 165, 32, 255);
            else if (listOfScores[1].UserScore < score)
                GUI.color = new Color32(211, 211, 211, 255);
            else if (listOfScores[2].UserScore < score)
                GUI.color = new Color32(222, 170, 136, 255);
            else
                GUI.color = Color.white;
        }
        else
            GUI.color = Color.white;



        Score = score;
        GUILayout.Label(" Score: " + score.ToString(), customButton);
    }

    void CreateObstacle()
    {
        Instantiate(rocks);
        score++;
    }
}