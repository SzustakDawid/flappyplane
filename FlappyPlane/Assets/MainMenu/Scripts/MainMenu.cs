﻿using UnityEngine;

public class MainMenu : MonoBehaviour
{

    public Texture backgroundTexture;

    public float guiPlacementY1;
    public float guiPlacementY2;
    public float guiPlacementY3;
    public float guiPlacementY4;
    public float guiPlacementY5;

    public float guiPlacementX1;
    public float guiPlacementX2;
    public float guiPlacementX3;
    public float guiPlacementX4;
    public float guiPlacementX5;


    void OnGUI()
    {
        string userLogin = PlayerPrefs.GetString("UserName");
        if (string.IsNullOrEmpty(userLogin))
        {
            Application.LoadLevel("CreateUser");
        }

        GUIStyle customButton = new GUIStyle("button");
        customButton.fontSize = (int)(Screen.height * 0.05f);

        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backgroundTexture);

        if (GUI.Button(new Rect(Screen.width * guiPlacementX1, Screen.height * guiPlacementY1, Screen.width * .5f, Screen.height * .1f), "Graj", customButton))
        {
            Application.LoadLevel("Play");
        }

        if (GUI.Button(new Rect(Screen.width * guiPlacementX2, Screen.height * guiPlacementY2, Screen.width * .5f, Screen.height * .1f), "Ranking", customButton))
        {
            Application.LoadLevel("HighScore");
        }

        if (GUI.Button(new Rect(Screen.width * guiPlacementX3, Screen.height * guiPlacementY3, Screen.width * .5f, Screen.height * .1f), "O autorze", customButton))
        {
            Application.LoadLevel("About");
        }

        if (GUI.Button(new Rect(Screen.width * guiPlacementX5, Screen.height * guiPlacementY5, Screen.width * .1f, Screen.height * .1f), "Gracz", customButton))
        {
            Application.LoadLevel("CreateUser");
        }

        if (GUI.Button(new Rect(Screen.width * guiPlacementX4, Screen.height * guiPlacementY4, Screen.width * .5f, Screen.height * .1f), "Wyjście", customButton))
        {
            Application.Quit();
        }
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
